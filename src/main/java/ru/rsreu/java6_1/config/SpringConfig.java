package ru.rsreu.java6_1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rsreu.java6_1.entity.ListUsers;
import ru.rsreu.java6_1.service.UserServicesImpl;

@Configuration
public class SpringConfig {

    @Bean
    public UserServicesImpl userServices() {
        return new UserServicesImpl();
    }

    @Bean
    public ListUsers listUsers() {
        return new ListUsers();
    }


}
