package ru.rsreu.java6_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import ru.rsreu.java6_1.entity.User;
import ru.rsreu.java6_1.service.UserService;

@Controller
@RequestMapping("/")
@SessionAttributes("loginUser")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String reg(Model model) {
        if (!model.containsAttribute("loginUser")) {
            return "index";
        } else {
            return "redirect:/users";
        }
    }

    @GetMapping("/users")
    public String getAllUsers(Model model){
        model.addAttribute("users",userService.getUsers());
        return "users";
    }

    @GetMapping("/addUser")
    public String addUser(Model model) {
        if (!model.containsAttribute("loginUser")) {
            return "addUser";
        } else {
            return "redirect:/users";
        }
    }

    @PostMapping("/addUser")
    public String addUser(@ModelAttribute("user") User user){
        userService.save(user);
        return "redirect:/";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("user") User user, Model model){
        if (userService.login(user)) {
            model.addAttribute("loginUser", user.getName());
            return "redirect:/users";
        } else {
            return "redirect:/";
        }
    }

    @PostMapping("/exit")
    public String exit(Model model, SessionStatus status) {
        if (model.containsAttribute("loginUser")) {
            status.setComplete();
            return "redirect:/";
        } else {
            return null;
        }
    }


}
