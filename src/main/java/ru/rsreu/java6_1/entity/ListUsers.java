package ru.rsreu.java6_1.entity;

import ru.rsreu.java6_1.entity.User;

import java.util.ArrayList;
import java.util.List;

public class ListUsers {

    private List<User> userList;

    public ListUsers() {
        userList = new ArrayList<>();
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
