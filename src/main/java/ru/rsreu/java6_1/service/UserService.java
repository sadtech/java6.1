package ru.rsreu.java6_1.service;

import ru.rsreu.java6_1.entity.User;

import java.util.List;

public interface UserService {

    List<User> getUsers();

    void save(User user);

    boolean login(User user);

}
