package ru.rsreu.java6_1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rsreu.java6_1.entity.ListUsers;
import ru.rsreu.java6_1.entity.User;

import java.util.List;

@Service
public class UserServicesImpl implements UserService {

    @Autowired
    ListUsers listUsers;

    @Override
    public List<User> getUsers() {
        return listUsers.getUserList();
    }

    @Override
    public void save(User newUser) {
        String searcher = newUser.getName();
        for (User user: listUsers.getUserList()) {
            if (user.getName().equals(searcher)) {
                return;
            }
        }
        listUsers.getUserList().add(newUser);
    }

    @Override
    public boolean login(User user) {
        for (User user1: listUsers.getUserList()) {
           if (user1.equals(user)) {
               return true;
           }
        }
        return false;
    }

}
